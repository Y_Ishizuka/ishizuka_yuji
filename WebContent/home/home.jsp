<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
</head>
<body>
	<h3>ホーム画面</h3>
	<c:if test="${ not empty loginUser }">
	    <div class="profile">
	        <div class="account">
	            ログイン@<c:out value="${loginUser.loginName}" />
	        </div>
	    </div>
	</c:if>
	<br />
	<c:if test="${!empty message }">
		${ message }
	</c:if>
	<form action="./AddPost"><br />
	<input type="submit" value="新規投稿" /> <br />
	</form>
	<form action="UserManagement"><br />
	<input type="submit" value="ユーザー管理" /> <br />
	</form>
	<br />
	<div class="my-posts">
	    <c:forEach items="${userMessages}" var="message">
	            <div class="my-message">
	                <div class="account-name">
	                    <span class="title"><c:out value="${message.title}" /></span>
	                    <span class="category"><c:out value="${message.category}" /></span>
	                    <span class="name"><c:out value="${message.userName}" /></span>
	                </div>
	                <div class="text"><c:out value="${message.text}" /></div>
	                <div class="date"><fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
	            </div>
	    </c:forEach>
	</div>
</body>
</html>