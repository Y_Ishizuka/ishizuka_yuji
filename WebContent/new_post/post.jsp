<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿画面</title>
</head>
<body>
	<c:if test="${ not empty loginUser }">
	    <div class="profile">
	        <div class="account">
	            ログイン@<c:out value="${loginUser.loginName}" />
	        </div>
	    </div>
	</c:if>
	<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		         <ul>
		             <c:forEach items="${errorMessages}" var="message">
		                 <li><c:out value="${message}" />
		             </c:forEach>
		         </ul>
     		</div>
     		<c:remove var="errorMessages" scope="session" />
 	</c:if>
 	<div class="form-area">
        <form action="AddPost" method="post">
            <h1>投稿内容</h1><br />
            <label for="title">件名</label> <input name="title" id="title" />(30文字まで)<br />
            <label for="category">カテゴリ</label> <input name="category" id="category" />(10文字まで)<br />
            <textarea name="contents" cols="100" rows="15"></textarea><br />
            （1000文字まで)<br />
            <input type="submit" value="投稿">
        </form>
	</div>

</body>
</html>