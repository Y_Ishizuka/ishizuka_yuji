<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログイン画面</title>
</head>
<body>
	<h3>ログイン</h3>

	<div class="main-contents">
		<c:if test="${ not empty message }">
		    <div class="message">
		        <ul>
		        	<li><c:out value="${message}" />
		        </ul>
		    </div>
		    <c:remove var="message" scope="session"/>
		</c:if>

		<form action="/Login" method="post"><br />
		    <label for="id">ユーザーID</label>
		    <input name="id" id="id"/> <br />
		    <label for="password">パスワード</label>
		    <input name="password" type="password" id="password"/> <br />
		    <input type="submit" value="ログイン" /> <br />
		    <a href="./">戻る</a>
		</form>
		<br />
		<div class="copyright"> Copyright(c)Yuji Ishizuka</div>
	 </div>
</body>
</html>