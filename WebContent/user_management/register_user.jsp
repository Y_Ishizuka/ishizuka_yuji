<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録画面</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
		    <div class="errorMessages">
		         <ul>
		             <c:forEach items="${errorMessages}" var="message">
		                 <li><c:out value="${message}" />
		             </c:forEach>
		         </ul>
     		</div>
     		<c:remove var="errorMessages" scope="session" />
	 	</c:if>
		 <form action="RegisterUser" method="post">
		    <br /> <label for="id">ユーザーID</label> <input name="id" id="id" /><br />
		    <label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
		    <label for="passwordConfirm">パスワード確認</label> <input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
		    <label for="name">名称</label> <input name="name" id="name" /> <br />
		    <label for="branch">支店</label> <input name="branch" type="number" id="branch" /> <br />
			<label for="position">部署・役職</label> <input name="position" type="number" id="position" /> <br />
			<label for="user">アカウント状態</label>
			<select name="user">
				<option value="1" >始動</option>
				<option value="0" >停止</option>
			</select> <br />
		    <br /> <input type="submit" value="登録" /> <br />
		 </form>
		 <br />
		 <div class="copyright">Copyright(c)Yuji Ishizuka</div>
	</div>
</body>
</html>