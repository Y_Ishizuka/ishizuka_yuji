<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<body>
	<h3>ユーザー管理</h3>
	<c:if test="${!empty message }">
	${ message }
	</c:if>

	<% List<User> list = (List<User>) request.getAttribute("userList"); %>

	<table border="1">
	<tr>
	<th>ユーザーID</th>
	<th>名称</th>
	<th>支店</th>
	<th>部署・役職</th>
	<th>アカウント状態</th>
	<th>編集</th>
	<th></th>
	</tr>
	<% for(User user : list) { %>
	<tr>
	<td><%= user.getId() %></td>
	<td><%= user.getName() %></td>
	<td><%= user.getBranchName() %></td>
	<td><%= user.getPositionName() %></td>
	<td><%= user.getUserCondition() %></td>
	<td>
	<form action="EditUser" method="post">
	<input type="hidden" name="userid" value="<%=user.getId() %>">
	<input type="submit" value="ユーザー編集" /></form>
	</td>
	<td>
	<form action="ChangeUserCondition" method="post" onSubmit="return check()">
	<input type="hidden" name="userid" value="<%=user.getId() %>">
	<input type="submit" value="状態変更" /></form>
	</td>
	</tr>
	<% } %>
	</table>

	<form action="RegisterUser"><br />
	<input type="submit" value="新規ユーザー登録" /> <br />
	</form>
	<form action="./"><br />
	<input type="submit" value="ホーム" /> <br />
	</form>

	<script type="text/javascript">
		function check(){
			if(window.confirm('アカウント状態を変更しますか？')){ // 確認ダイアログを表示
				return true; // 「OK」時は送信を実行
			}
			else{ // 「キャンセル」時の処理
				window.alert('キャンセルされました'); // 警告ダイアログを表示
				return false; // 送信を中止
			}
		}
	</script>
</body>
</html>