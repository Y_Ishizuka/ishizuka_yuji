<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.User"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集画面</title>
</head>
<body>
	<h3>ユーザー編集</h3>
	<%
	User user;
	if (session.getAttribute("editingData") == null){
		System.out.println("new editingData");
		session.setAttribute("editingData", request.getAttribute("user"));
		user = (User) request.getAttribute("user");
		System.out.println(user.getId());
		session.setAttribute("previousUserId", user.getId());
	} else {
		System.out.println("already");
		user = (User) session.getAttribute("editingData");
	}
	%>
	<div class="main-contents">
		 <c:if test="${ not empty errorMessages }">
		     <div class="errorMessages">
		         <ul>
		             <c:forEach items="${errorMessages}" var="message">
		                 <li><c:out value="${message}" />
		             </c:forEach>
		         </ul>
		     </div>
		     <c:remove var="errorMessages" scope="session" />
		 </c:if>
		 <form action="ChangeUser" method="post">
		    <br /><label for="id">ユーザーID</label> <input name="id" id="id" value="<%=user.getId()%>" /> <br />
		    <label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
		    <label for="passwordConfirm">パスワード確認</label> <input name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
		    <label for="name">名称</label> <input name="name" id="name" value="<%=user.getName()%>" /> <br />
		    <label for="branch">支店</label> <input name="branch" type="number" id="branch" value="<%=user.getBranch()%>" /> <br />
			<label for="position">部署・役職</label> <input name="position" type="number" id="position" value="<%=user.getPosition()%>" /> <br />
			<c:set var="num" value="<%=user.getUser() %>"/>
			<c:if test="${ num == 1}">
				<label for="user">アカウント状態</label>
				<select name="user"> <option value="1" selected >始動</option> <option value="0" >停止</option>
				</select> <br />
			</c:if>
			<c:if test="${ num != 1}">
				<label for="user">アカウント状態</label>
				<select name="user"> <option value="1" >始動</option> <option value="0" selected>停止</option>
				</select> <br />
			</c:if>
		    <br />
		    <input type="hidden" name="previousUserId" value="<%=session.getAttribute("previousUserId") %>">
		    <input type="submit" value="変更" /> <br />
		 </form>
		 <br />
		 <div class="copyright">Copyright(c)Yuji Ishizuka</div>
	</div>
</body>
</html>