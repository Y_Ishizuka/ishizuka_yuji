package beans;

public class User {
//	@NotNull
//	@Size(min = 6, max = 20)
//	@Pattern(regexp = "^[a-zA-Z0-9]+$")
	private String id;
//	@NotNull
//	@Size(min = 6, max = 20, message = "{min}文字以上{max}字以下")
//	@Pattern(regexp = "^[\\p{Alnum}|\\p{Punct}]+$")
	private String password;
//	@Size(min = 1, max = 10)
	private String name;
//	@Digits(integer = 3, fraction = 0)
//	@Min(value = 0)
	private int branch;
//	@Digits(integer = 3, fraction = 0)
//	@Min(value = 0)
	private int position;
//	@Max(value = 1)
//	@Min(value = 0)
	private int user;
	private String branchName;
	private String positionName;
	private String userCondition;

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getUserCondition() {
		return userCondition;
	}
	private void setUserCondition(int Condition) {
		if (Condition == 1) {
			this.userCondition = "始動";
		} else {
			this.userCondition = "停止";
		}

	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
		setUserCondition(user);
	}
}
