package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;


public class CommentDao {
	static Connection connection = null;
	static PreparedStatement statement = null;

	private static Connection getConnection() {
		String driver = "com.mysql.jdbc.Driver";
		// 「database_name」は各環境に合わせます
        String url = "jdbc:mysql://localhost/ishizuka_yuji";
        // MySQLに接続する際のユーザー名(デフォルトはroot)
        String user = "root";
        // MySQLに接続する際のパスワード(デフォルトはなし)
        String password = "m08UoA10y";
        Connection connection = null;
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, password);
	        connection.setAutoCommit(false);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}

	private static void closeConnection(PreparedStatement statement, Connection connection) {
		if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}


}
