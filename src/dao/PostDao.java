package dao;

import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;

import beans.LoginUser;
import utils.DBUtil;

public class PostDao {
	static Connection connection = null;
	static PreparedStatement statement = null;
	public static void insert(HttpServletRequest request, LoginUser loginUser) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("INSERT INTO posts (title, content, category, created_at, user_id) "
					+"VALUES (?, ?, ?, CURRENT_TIMESTAMP, ?)");
			statement.setString(1, request.getParameter("title"));
			statement.setString(2, request.getParameter("contents"));
			statement.setString(3, request.getParameter("category"));
			statement.setString(4, loginUser.getLoginId());
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("投稿完了");
	            connection.commit();
	        } else {
	            System.out.println("投稿失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}
}
