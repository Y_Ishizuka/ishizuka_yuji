package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

import beans.User;
import utils.DBUtil;
import beans.LoginUser;

public class UserDao{
	static Connection connection = null;
	static PreparedStatement statement = null;

	public static LoginUser checkUser(String id, String password) {
		LoginUser loginUser = new LoginUser();
		try {
			connection = DBUtil.getConnection();
	        statement = connection.prepareStatement("SELECT * FROM users WHERE id = ? AND password = ?");
	        statement.setString(1, id);
	        statement.setString(2, password);
	        ResultSet rs = statement.executeQuery();
	        while(rs.next()) {
				String resultId = rs.getString("id");
				String resultPass = rs.getString("password");
				if (resultId.equals(id) && resultPass.equals(password)) {
					loginUser.setLoginId(resultId);
					loginUser.setLoginName(rs.getString("name"));
				}
				else {
					loginUser = null;
				}
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		if (loginUser.getLoginId() == null || loginUser.getLoginName() == null) {
			return null;
		} else {
			return loginUser;
		}
	}

	public static User findUser(String id) {
		User user = new User();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
	        statement.setString(1, id);
	        ResultSet rs = statement.executeQuery();
	        rs.next();
	        user.setId(id);
	        user.setPassword(rs.getString("password"));
	        user.setName(rs.getString("name"));
	        user.setBranch(Integer.parseInt(rs.getString("branch")));
	        user.setPosition(Integer.parseInt(rs.getString("position")));
	        user.setUser(Integer.parseInt(rs.getString("user")));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return user;
	}

	public List<User> getAllUser() {
		List<User> userList = new ArrayList<User>();
		try {
			User user = null;
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT users.id, users.name, branches.branch_name, "
					+ "positions.position_name, users.user "
					+ "from users inner join branches on users.branch = branches.branch_id "
					+ "inner join positions on users.position = positions.position_id;");
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setId(rs.getString("id"));
				user.setName(rs.getString("name"));
				user.setBranchName(rs.getString("branch_name"));
				user.setPositionName(rs.getString("position_name"));
				user.setUser(Integer.parseInt(rs.getString("user")));
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return userList;
	}

	public void changeUserCondition(String id) {
		try {
			String condition = null;
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT user FROM users WHERE id = ?");
			statement.setString(1, id);
			ResultSet rs = statement.executeQuery();
			rs.next();
			condition = rs.getString("user");
			if (condition.equals("1")) {
				statement = connection.prepareStatement("UPDATE users SET user = 0 WHERE id = ?");
				statement.setString(1, id);
			} else {
				statement = connection.prepareStatement("UPDATE users SET user = 1 WHERE id = ?");
				statement.setString(1, id);
			}
	        int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("変更成功");
	            connection.commit();
	        } else {
	            System.out.println("変更失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static void insert(User user) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("INSERT INTO users (id, password, name, branch, position, user) "
					+"VALUES (?, ?, ?, ?, ?, ?)");
			statement.setString(1, user.getId());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getName());
			statement.setString(4, Integer.toString(user.getBranch()));
			statement.setString(5, Integer.toString(user.getPosition()));
			statement.setString(6, Integer.toString(user.getUser()));
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("登録完了");
	            connection.commit();
	        } else {
	            System.out.println("登録失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public static void edit(User user, String previousUserId) {
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("UPDATE users SET id = ?, password = ?, name = ?, "
					+ "branch = ?, position = ?, user = ? WHERE id = ?");
			statement.setString(1, user.getId());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getName());
			statement.setString(4, Integer.toString(user.getBranch()));
			statement.setString(5, Integer.toString(user.getPosition()));
			statement.setString(6, Integer.toString(user.getUser()));
			statement.setString(7, previousUserId);
			int updateCount = statement.executeUpdate();
	        if (updateCount == 1) {
	            System.out.println("編集完了");
	            connection.commit();
	        } else {
	            System.out.println("編集失敗");
	            connection.rollback();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
	}

	public Boolean isEditer(LoginUser user) {
		boolean result = false;
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT position FROM users WHERE id = ?");
			statement.setString(1, user.getLoginId());
			ResultSet rs = statement.executeQuery();
	        rs.next();
	        if (rs.getInt("position") == 101) {
	        	result = true;
	        } else {
	        	result = false;
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return result;
	}
}