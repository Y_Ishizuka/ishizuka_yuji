package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import utils.DBUtil;

public class UserMessageDao {
	private static Connection connection = null;
	private static PreparedStatement statement = null;
	public static List<UserMessage> getMessages() {
		List<UserMessage> allMessages = new ArrayList<UserMessage>();
		try {
			connection = DBUtil.getConnection();
			statement = connection.prepareStatement("SELECT posts.id, posts.title, posts.content, posts.category, "
					+ "posts.created_at, users.name FROM posts INNER JOIN users ON posts.user_id = users.id "
					+"ORDER BY posts.created_at DESC LIMIT 100");
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("title"));
				message.setText(rs.getString("content"));
				message.setCategory(rs.getString("category"));
				message.setTitle(rs.getString("title"));
				message.setCreated_date(rs.getTimestamp("created_at"));
				message.setUserName(rs.getString("name"));
				allMessages.add(message);
			}
		} catch (SQLException e) {

		} finally {
			DBUtil.closeConnection(statement, connection);
		}
		return allMessages;
	}
}
