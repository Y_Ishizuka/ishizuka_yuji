package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;
import utils.ValidateUserInfo;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/RegisterUser")
public class RegisterUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/user_management/register_user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<String> messages = new ArrayList<String>();
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("passwordConfirm");
		boolean passCheck = password1.equals(password2);
		messages = ValidateUserInfo.validate(request, messages);
		if (messages.isEmpty() && passCheck) {
			User user = new User();
			user.setId(request.getParameter("id"));
			user.setPassword(CipherUtil.encrypt(request.getParameter("password")));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));
			user.setUser(1);
			UserDao.insert(user);
			response.sendRedirect("UserManagement");
		} else {
			if (!passCheck) {
				messages.add("パスワードと確認パスワードが一致しません");
			}
			request.getSession().setAttribute("errorMessages", messages);
			response.sendRedirect("RegisterUser");
		}
	}
}
