package servlet;

import java.util.ArrayList;
import java.util.List;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LoginUser;
import dao.PostDao;

/**
 * Servlet implementation class AddPostServlet
 */
@WebServlet("/AddPost")
public class AddPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddPostServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/new_post/post.jsp").forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<String> messages = new ArrayList<String>();
		messages = validatePost(request);
		if (messages.isEmpty()) {
			PostDao.insert(request, (LoginUser)request.getSession().getAttribute("loginUser"));
			response.sendRedirect("./");
		} else {
			request.getSession().setAttribute("errorMessages", messages);
			response.sendRedirect("AddPost");
		}
	}

	private List<String> validatePost(HttpServletRequest request) {
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		String contents = request.getParameter("contents");
		List<String> messages = new ArrayList<String>();
		if (title.isEmpty()) {
			messages.add("件名を入力して下さい");
		}
		if (title.length() > 30) {
			messages.add("件名は30文字以下です");
		}
		if (category.isEmpty()) {
			messages.add("件名を入力して下さい");
		}
		if (category.length() > 10) {
			messages.add("カテゴリーは10文字以下です");
		}
		if (contents.isEmpty()) {
			messages.add("本文を入力して下さい");
		}
		if (title.length() > 1000) {
			messages.add("本文は1000文字以下です");
		}
		return messages;
	}
}
