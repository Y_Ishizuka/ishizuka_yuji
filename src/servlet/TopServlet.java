package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LoginUser;
import beans.UserMessage;
import dao.UserMessageDao;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet("/index.jsp")
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		LoginUser user = (LoginUser) request.getSession().getAttribute("loginUser");
        if (user != null) {
            List<UserMessage> userMessages = UserMessageDao.getMessages();
            request.setAttribute("userMessages", userMessages);
    		request.getRequestDispatcher("/home/home.jsp").forward(request, response);
        } else {
    		request.getRequestDispatcher("./Login").forward(request, response);
        }

	}

}
