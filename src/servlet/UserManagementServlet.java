package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.LoginUser;
import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserManagement
 */
@WebServlet("/UserManagement")
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserManagementServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		UserDao userdao = new UserDao();
		boolean editer = userdao.isEditer((LoginUser)request.getSession().getAttribute("loginUser"));
		if (!editer) {
			request.setAttribute("message", "このアカウントはユーザー管理権限がありません");
			request.getRequestDispatcher("./").forward(request, response);
		} else {
			List<User> userList = userdao.getAllUser();
			request.getSession().setAttribute("editingData", null);
			request.setAttribute("userList", userList);
			request.getRequestDispatcher("/user_management/user_management.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/user_management/register_user.jsp").forward(request, response);
		//
	}

}
