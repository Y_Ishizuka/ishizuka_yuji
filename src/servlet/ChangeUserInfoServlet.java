package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import beans.User;
import utils.CipherUtil;
import utils.ValidateUserInfo;

/**
 * Servlet implementation class ChangeUserInfoServlet
 */
@WebServlet("/ChangeUser")
public class ChangeUserInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeUserInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/user_management/edit_user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		User user;
		List<String> messages = new ArrayList<String>();
		String previousUserId = request.getParameter("previousUserId");
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("passwordConfirm");
		if (password1.isEmpty() && password2.isEmpty()) {
			user = (User) request.getSession().getAttribute("editingData");
			String noChangePassword = user.getPassword();
			messages = ValidateUserInfo.validateForUserEdit(request, messages);
			if (messages.isEmpty()) {
				user = new User();
				user.setId(request.getParameter("id"));
				user.setPassword(noChangePassword);
				user.setName(request.getParameter("name"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setUser(Integer.parseInt(request.getParameter("user")));
				UserDao.edit(user, previousUserId);
				response.sendRedirect("UserManagement");
			} else {
				request.getSession().setAttribute("errorMessages", messages);
				response.sendRedirect("ChangeUser");
			}
		} else {
			messages = ValidateUserInfo.validate(request, messages);
			boolean passCheck = password1.equals(password2);
			if(passCheck && messages.isEmpty()) {
				user = new User();
				user.setId(request.getParameter("id"));
				user.setPassword(CipherUtil.encrypt(request.getParameter("password")));
				user.setName(request.getParameter("name"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setUser(Integer.parseInt(request.getParameter("user")));
				UserDao.edit(user, previousUserId);
				response.sendRedirect("UserManagement");
			} else {
				if(!passCheck) {
					messages.add("パスワードと確認パスワードが一致しません");
				}
				request.getSession().setAttribute("errorMessages", messages);
				response.sendRedirect("ChangeUser");
			}
		}
	}

}
