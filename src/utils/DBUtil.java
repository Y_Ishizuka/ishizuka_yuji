package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBUtil {
	public static Connection getConnection() {
		final String driver = "com.mysql.jdbc.Driver";
		// 「database_name」は各環境に合わせます
		final String url = "jdbc:mysql://localhost/ishizuka_yuji";
        // MySQLに接続する際のユーザー名(デフォルトはroot)
		final String user = "root";
        // MySQLに接続する際のパスワード(デフォルトはなし)
		final String password = "m08UoA10y";
        Connection connection = null;
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, password);
	        connection.setAutoCommit(false);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}

	public static void closeConnection(PreparedStatement statement, Connection connection) {
		if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
}
