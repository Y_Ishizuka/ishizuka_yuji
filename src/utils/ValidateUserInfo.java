package utils;

import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class ValidateUserInfo {
	public static List<String> validate(HttpServletRequest request, List<String> message){
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position = request.getParameter("position");
		if(id == null || id.isEmpty()) {
			message.add("ユーザーIDは必須入力項目です");
		}
		if (id.length() < 6 || id.length() > 20) {
			message.add("ユーザーIDは6〜20字以内です");
		}
		if (!id.matches("^[\\w]+$")) {
			message.add("ユーザーIDは半角英数字で入力してください");
		}
		if (!password.isEmpty() && (password.length() < 6 || password.length() > 20)) {
			message.add("パスワードは6〜20字以内です");
		}
		if (!password.matches("^[-@+*;:#$%&\\w]+$")) {
			message.add("パスワードは半角英数記号で入力してください");
		}
		if (name.length() > 10 || name == null || name.isEmpty()) {
			message.add("名称は10字以内で入力してください");
		}
		if (branch.length() > 3 || branch == null || branch.isEmpty()) {
			message.add("支店コードは3桁以内で入力してください");
		}
		if (position.length() > 3 || position == null || position.isEmpty()) {
			message.add("部署・役職コードは3桁以内で入力してください");
		}
		return message;
	}

	public static List<String> validateForUserEdit(HttpServletRequest request, List<String> message){
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String branch = request.getParameter("branch");
		String position = request.getParameter("position");
		if(id == null || id.isEmpty()) {
			message.add("ユーザーIDは必須入力項目です");
		}
		if (id.length() < 6 || id.length() > 20) {
			message.add("ユーザーIDは6〜20字以内です");
		}
		if (!id.matches("^[\\w]+$")) {
			message.add("ユーザーIDは半角英数字で入力してください");
		}
		if (name.length() > 10 || name == null || name.isEmpty()) {
			message.add("名称は10字以内で入力してください");
		}
		if (branch.length() > 3 || branch == null || branch.isEmpty()) {
			message.add("支店コードは3桁以内で入力してください");
		}
		if (position.length() > 3 || position == null || position.isEmpty()) {
			message.add("部署・役職コードは3桁以内で入力してください");
		}
		return message;
	}
}
